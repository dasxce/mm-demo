environments {     
    
    'hospital-a-staging' { 
        versionSuffix = "Hospital-A-RC"
    }
    'hospital-a-production' { 
        versionSuffix = "Hospital-A-FINAL"
    }
    
    'hospital-b-staging' { 
        versionSuffix = "Hospital-B-RC"
    }
    'hospital-b-production' { 
        versionSuffix = "Hospital-B-FINAL"
    }
}